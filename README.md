Adderall is a stimulant medication which is often used to treat illnesses such as Attention Deficit Hyperactivity Disorder (ADHD) and the sleeping disorder, narcolepsy. Adderall is also sometimes used by students to improve cognitive function in order to help them study. Due to it's physical performance-enhancing capacity it is also sometimes used by athletes, despite being prohibited by most sports organizations. 
 
The drug is sometimes used recreationally due to the fact that it increases libido and can also induce feelings of euphoria. Adderall is essentially a psychostimulant made up of mostly two different types of amphetamine salts; dextroamphetamine and levoamphetamine. It works by increasing levels of the dopamine and norepinephrine neurotransmitters in the brain and can also have analgesic effects working effectively as a painkiller. 
 
**Medicinal Benefits** 

In the medical field, Aderall has been shown to have had an extremely positive and beneficial effect on the health of 80% of patients diagnosed with [ADHD](https://www.healthline.com/health/adhd/signs) who were being treated with the drug. Although long-term use of amphetamines is known to adversely affect the health of most animals and cause nerve damage, it has been shown to improve nerve growth and brain development in humans which is why it has become such a popular choice among students. 
 
Adderall was also shown to decrease abnormalities in both brain function and structure, improving the function of many areas of the brain including the right caudate nucleus of the basal ganglia. Additionally, it was found that long-term use of Adderall at a safe dosage is both safe and effective and in a trial that lasted nine months, the IQ of children treated with Adderall rose by 4.5 points overall and their attention, focus and behavior improved. 
 
**Performance Enhancing Effects** 

Adderall has been shown to be proficient at not just improving human physical performance but also at improving cognition, memory, attention and inhibition in not only those who suffer with ADHD but in all humans, as long as it is used at a safe dose. 
 
Athletically, Adderall has been shown to increase alertness, endurance, muscle strength, acceleration and reaction time. Improved endurance and reaction time is caused by reuptake inhibition and effluxion of dopamine in the central nervous system whereas improved cognition is due to indirect activation of dopamine receptors in the prefrontal cortex. 
 
**Availability** 

Adderall is available in two different types; immediate-release tablets and extended-release capsules. The difference between the tablets and the capsules being that the extended-release capsules, which come under the brand name Adderall XR, are designed to be more therapeutic and have the same effect of taking two immediate-release tablets four hours apart. 
 
Adderall and other [best natural adhd supplements](http://www.adderallonline.org/adderall-alternatives/) is mostly used to treat patients suffering with ADHD and is therefore usually prescribed by a doctor therefore, if you do not suffer with ADHD, then you will need to buy Adderall online from an online pharmacy. 
